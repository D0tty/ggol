#include <chrono>
#include <cstdlib>
#include <iostream>

#include "screen.hh"

extern void life_and_death_gpu_wrapper(std::vector<int>& cells, size_t width, size_t height);

Screen::Screen(size_t width, size_t height)
    : width_(width)
    , height_(height)
{
    cells_ = std::vector(width * height, DEAD);
    SDL_CreateWindowAndRenderer(width, height, 0, &window_, &renderer_);
    //init sdl here
}

Screen::~Screen()
{
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

void Screen::populate(size_t cells_nb) noexcept
{
    for (; cells_nb; --cells_nb)
    {
        auto x = std::rand() % width_;
        auto y = std::rand() % height_;

        cells_[width_ * y + x] = ALIVE;
    }
}

void Screen::render() noexcept
{
    std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();

    for (size_t x = 0; x < width_; ++x)
    {
        for (size_t y = 0; y < height_; ++y)
        {
            if (cells_[y * width_ + x] == ALIVE)
                SDL_SetRenderDrawColor(renderer_, 255, 0, 0, 255);
            else if (cells_[y * width_ + x] == NEWBORN)
                SDL_SetRenderDrawColor(renderer_, 0, 255, 255, 255);
            else
                SDL_SetRenderDrawColor(renderer_, 0, 0, 0, 255);

            SDL_RenderDrawPoint(renderer_, x, y);
        }
    }

    SDL_RenderPresent(renderer_);
    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
    auto elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count();
    std::cout << "Elapsed render: " << elapsed_seconds << " ms" << std::endl;
}

void Screen::life_and_death() noexcept
{
    std::vector<int> copy_cells = cells_;

    std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
    for (size_t x = 1; x < width_ - 1; ++x)
    {
        for (size_t y = 1; y < height_ - 1; ++y)
        {
            unsigned count = 0;
            auto& state = copy_cells[y * width_ + x];
            count += cells_[y * width_ + x - 1] != DEAD;
            count += cells_[y * width_ + x + 1] != DEAD;
            count += cells_[(y + 1) * width_ + x] != DEAD;
            count += cells_[(y + 1) * width_ + x + 1] != DEAD;
            count += cells_[(y + 1) * width_ + x - 1] != DEAD;
            count += cells_[(y - 1) * width_ + x] != DEAD;
            count += cells_[(y - 1) * width_ + x + 1] != DEAD;
            count += cells_[(y - 1) * width_ + x - 1] != DEAD;

            if (count == 3)
                state = state == NEWBORN ? ALIVE : NEWBORN;
            else if (count < 2 || count > 3)
                state = DEAD;
        }
    }

    cells_.swap(copy_cells);

    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
    auto elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count();
    std::cout << "Elapsed life_and_death: " << elapsed_seconds << " ms" << std::endl;
}

void Screen::gpu_life_and_death() noexcept
{
    std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();

    life_and_death_gpu_wrapper(cells_, width_, height_);

    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
    auto elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count();
    std::cout << "Elapsed life_and_death: " << elapsed_seconds << " ms" << std::endl;
}
