CXX=g++
CPPFLAGS=-I/opt/cuda/include -L/opt/cuda/lib64 -D_GNU_SOURCE=1 \
		 -D_REENTRANT -I/usr/include/SDL2
CXXFLAGS=-Wall -Wextra -std=c++17 -O3
LDLIBS=-lcudart -lSDL2
BIN=gol
OBJS=main.o screen.o
CUSRC=gpu_life_and_death.cu
CUOBJS=$(CUSRC:.cu=.o)

all:$(BIN)

$(BIN):$(OBJS) $(CUOBJS)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

cuda:$(CUOBJS)

$(CUOBJS):$(CUSRC)
	nvcc -c $(CUSRC)

clean:
	$(RM) $(OBJS) $(CUOBJS) $(BIN)

.PHONY: all clean cuda
