- Check that you have SDL2 correctly installed
- If you don't have cuda installed, remove the cuda dependencies in the Makefile
(i.e remove $CUSRC)
- If you have cuda installed, check that the include and link path in $CPPFLAGS
are correctly set


Usage:
```C++
sh$ make
sh$ ./gol [--gpu]
```
